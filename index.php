<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>108 шагов по воде</title>
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/meyer-reset/2.0/reset.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>

<img src="img/bg.jpg" alt="" class="body-bg"/>

<div class="main">
    <h3>Здоровье, красота, молодость</h3>
    <h1>108 шагов по воде</h1>
    <h3>Секретная практика тибетских монахов для здоровья и долголетия</h3>
    <h4>Чек-лист в PDF-формате</h4>

    <div class="wrap">
        <div class="left-container">
            <h3>
                Получите ЧЕК-ЛИСТ, примените<br>простые, пошаговые действия <br>и у Вас будет:
            </h3>
            <ul>
                <li>Хорошее настроение;</li>
                <li>Прекрасное самочувствие;</li>
                <li>Мощный заряд жизненной <br>энергии;</li>
                <li>Простой и доступный <br>способ оздоровления; </li>
                <li>Очищение от негатива.</li>
            </ul>
        </div>
        <div class="form">
            <?php include 'form.php'; ?>
        </div>
        <div class="clear"></div>
        <?php include 'pluso.php'; ?>
    </div>
</div>

<div class="footer">
    <p>Все права защищены. Copyright © 2014. Людмила Смольникова</p>
</div>

<script src="http://cdn.jsdelivr.net/jquery/2.1.1/jquery.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>