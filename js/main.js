$(document).ready(function(){
    function footerToBottom() {
        var browserHeight = $(window).height(),
            footerOuterHeight = $('.footer').outerHeight(true),
            mainHeightMarginPaddingBorder = $('.main').outerHeight(true) - $('.main').height();
        $('.main').css({
            'min-height': browserHeight - footerOuterHeight * 2 - mainHeightMarginPaddingBorder
        });
    };
    footerToBottom();
    $(window).resize(function () {
        footerToBottom();
    });
});
